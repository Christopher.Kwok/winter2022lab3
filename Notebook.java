public class Notebook{
	
	public String brand;
	public String color;
	public int numOfPages;
	
	public void ripPage(){
		if(numOfPages > 0){
			this.numOfPages--;
			System.out.println("You ripped a page out of the notebook! There are " + this.numOfPages + " left!");
		}else{ //Added so that it cannot rip into negative numbers.			
			System.out.println("There are no more pages to rip out!");
		}
	}
}