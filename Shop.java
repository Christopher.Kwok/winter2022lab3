import java.util.Scanner;

public class Shop{
	
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		
		Notebook[] shelfOne = new Notebook[4];
		
		for(int i = 0; i < 4; i++){
			shelfOne[i] = new Notebook();
			
			System.out.println("What brand of notebook is it?");
			shelfOne[i].brand = sc.nextLine();
			System.out.println("What color is the cover of the notebook?");
			shelfOne[i].color = sc.nextLine();
			System.out.println("How many pages does this notebook have?");
			shelfOne[i].numOfPages = Integer.parseInt(sc.nextLine());
			System.out.println(""); //To create a separation between notebook inputs.
		}
		//To grab the last item in my array I an using index positio [shelfOne.length-1] so that I can have an array of any length.
		System.out.println("The last notebook is from the brand " + shelfOne[shelfOne.length-1].brand + " with a " + shelfOne[shelfOne.length-1].color + " cover and has " + shelfOne[shelfOne.length-1].numOfPages + " pages.");
		
		//Running ripPage() 5 times to show it updates properly and can keep working.
		shelfOne[shelfOne.length-1].ripPage();
		shelfOne[shelfOne.length-1].ripPage();
		shelfOne[shelfOne.length-1].ripPage();
		shelfOne[shelfOne.length-1].ripPage();
		shelfOne[shelfOne.length-1].ripPage();
	}
	
}